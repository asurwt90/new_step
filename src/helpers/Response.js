exports.sendAccepted = function(res, data) {    
    return res.status(202).send(data);
};

exports.sendCreated = function(res, data) {
  return res.status(200).send(data);
};

exports.sendBadRequest = function(res, message) {
  return res.status(400).send({
    success: false,
    error: message,
    response_id : null
  });
};

exports.sendUnauthorized = function(res, message) {
  return res.status(401).send({
    status: false,
    error: message
  });
};

exports.sendForbidden = function(res, message = null) {
  return res.status(403).send({ 
    success: false,
    error: (message!=null ? message : 'You do not have rights to access this resource.')
  });
};

exports.sendNotFound = function(res, message) {
  return res.status(404).send({
    success: false,
    error: message ? message : "Resource not found."
  });
};

exports.sendInternalServer = function(res) {
  return res.status(500).send({
    success: false,
    error: 'Something went wrong. Please try again later.'
  });
};

exports.setHeadersForCORS = function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, X-Access-Token, Content-Type, Accept");
  next();
}