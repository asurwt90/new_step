const response = require("../helpers/Response");

var test_data = require('../../config/test_data.json');
var single_data = require('../../config/single_data.json');

exports.show = function (req, res) {
    // console.log(process.env);
    if(req.params.id){
        response.sendAccepted(res, {
            "message": "Your request is successfully accepted.",
            "data": single_data,
            "success": true
        });
    }else{
        response.sendAccepted(res, {
            "message": "Your request is successfully accepted.",
            "data": test_data,
            "success": true
        });
    }
};

exports.store = function(req, res){
    response.sendAccepted(res, {
        "message": "Your request is successfully accepted.",
        "data": req,
        "success": true
    });
};

exports.update = function(req, res){
    response.sendAccepted(res, {
        "message": "Your request is successfully accepted.",
        "data": req.body,
        "success": true
    });
};