/*
* user routes
*/

var express = require('express');
var router = express.Router();

// Controllers
const UserController = require("../src/controllers/UserController");

// routes
router.get("/", UserController.show);
router.get("/:id", UserController.show);
// router.post('/create', UserController.store);
// router.put('/:id', UserController.update);

/* Create new user. */
router.post('/',function(req,res,next){
  res.json(req.body);
});

/* Update user by id. */
// router.put('/:id',function(req,res,next){
//   res.json({
//     "status": "Success",
//     "code": 200,
//     "message":"Resource updated successfully."
//   });
// });

module.exports = router;
